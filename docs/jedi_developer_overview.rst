===========================
О том, что стоит за питоном
===========================

Начнем с самого начала (от камня).

    - Китайская компания Espressif Systems родила чип `ESP32-D0WDQ6 <https://www.espressif.com/en/products/hardware/esp32/overview>`_ (`ESP32 Datasheet <https://www.espressif.com/sites/default/files/documentation/esp32_datasheet_en.pdf>`_) и на основе данного чипа создала модуль `ESP32-WROVER <https://docs.espressif.com/projects/esp-idf/en/latest/hw-reference/modules-and-boards.html>`_ (используется в железке), особенностью которого является наличие 8Мб оперативной памяти (PSRAM). (`ESP32-WROVER Datasheet <https://espressif.com/sites/default/files/documentation/esp32-wrover_datasheet_en.pdf>`_)

        .. figure:: _static/esp32-wrover.jpg

    - Для облегчения разработки софта под данный чип, компанией Espressif был создан фреймворк `ESP-IDF <https://docs.espressif.com/projects/esp-idf/en/latest/index.html>`_ (Espressif IoT Development Framework) основаный на фришной операционной системе реального времени FreeRTOS. ESP-IDF строится из модулей, и для конфигурирования использует графический инструмент kconfig (как при конфигурировании ядра linux).  
    - Посмотрев на всю эту красоту (а главное - много памяти), некто loboris, замутил проект `"MicroPython for ESP32 with psRAM support" <https://github.com/loboris/MicroPython_ESP32_psRAM_LoBo>`_ в котором оформил оригинальный `MicroPython <http://www.micropython.org/>`_ в виде модуля к ESP-IDF, а также написал много кода, позволяющего юзать возможности системы на чипе, из питона (`Wiki <https://github.com/loboris/MicroPython_ESP32_psRAM_LoBo/wiki>`_).

Вот такие пирожки с котятами. Зависимости не хилые, но думаю теперь вам понятно откуда растут ноги у девайса.

Да, чуть не забыл, `клон репозитория <https://github.com/vrubel/MicroPython_ESP32_psRAM_LoBo/tree/indicator-fun>`_ MicroPython_ESP32_psRAM_LoBo понадобился потому, что в прошивку был добавлен собственный Python модуль - драйвер датчика чистоты воздуха `SGP30 <https://www.sensirion.com/en/environmental-sensors/gas-sensors/multi-pixel-gas-sensors/>`_. Модуль драйвера - обертка в Python `оригинальной библиотеки от вендора <https://github.com/Sensirion/embedded-sgp>`_. А также там лежат `готовые прошивки <https://github.com/vrubel/MicroPython_ESP32_psRAM_LoBo/tree/indicator-fun/MicroPython_BUILD/firmware/indicator_fun>`_ для модуля WROVER (о том как их прошить, рассказано `тут <self_build.html#id4>`_ ).


