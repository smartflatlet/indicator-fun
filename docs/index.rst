.. INDICATOR-FUN documentation master file, created by
   sphinx-quickstart on Wed Jun 13 18:58:20 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

=====================
Прикольный светильник
=====================

.. image:: _static/general_view.jpg

.. toctree::
   :maxdepth: 2
   :caption: Содержание:

   documentation
   translation_of_third-party_docs/third-party_documentation
   elephants_distribution
   own_module
   
