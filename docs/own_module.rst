Пример создания собственного модуля Python на языке Си 
======================================================
Создание собствееного python модуля на языке C будет продемонстрировано на примере модуля indicator, созданного в рамках проекта indicator-fun. В примере будет показана реализация модуля содержащего класс IFunNeopixel, который содержит метод класса ``add`` (возвращающий, не смотря на название :-), удвоенное значение переданного ему параметра). 

Пример реализации модуля оформлен в виде комита `Minimum definition for own module <https://github.com/vrubel/MicroPython_ESP32_psRAM_LoBo/commit/57227919ffa93f426fb21212a5d949ea92f2d9f5>`_ в репозитории с прошивкой микропитона для esp32.


Код реализации модуля
---------------------
Весь наш код размещен в одном файле: ``/MicroPython_BUILD/components/micropython/esp32/modifunneopixel.c`` (название от "**mod**\ ule\  **i**\ ndicator-\ **fun** **neopixel**")

В файле определены:

    - `Структура для хранения данных экземпляра класса <https://github.com/vrubel/MicroPython_ESP32_psRAM_LoBo/blob/57227919ffa93f426fb21212a5d949ea92f2d9f5/MicroPython_BUILD/components/micropython/esp32/modifunneopixel.c#L42>`_. 
      *Структура пустая, не содержит полей, т.к. наш пример реализует простую функцию, не обращающуюся с данным экзмпляра класса.*
    - `Метод печати экземпляра класса <https://github.com/vrubel/MicroPython_ESP32_psRAM_LoBo/blob/57227919ffa93f426fb21212a5d949ea92f2d9f5/MicroPython_BUILD/components/micropython/esp32/modifunneopixel.c#L52>`_. *Обязательна для определения класса*
    - `Конструктор класса <https://github.com/vrubel/MicroPython_ESP32_psRAM_LoBo/blob/57227919ffa93f426fb21212a5d949ea92f2d9f5/MicroPython_BUILD/components/micropython/esp32/modifunneopixel.c#L63>`_ *Обязателен для определения класса*
    - `Метод класса <https://github.com/vrubel/MicroPython_ESP32_psRAM_LoBo/blob/57227919ffa93f426fb21212a5d949ea92f2d9f5/MicroPython_BUILD/components/micropython/esp32/modifunneopixel.c#L90>`_
    - `Таблица - список членов класса <https://github.com/vrubel/MicroPython_ESP32_psRAM_LoBo/blob/57227919ffa93f426fb21212a5d949ea92f2d9f5/MicroPython_BUILD/components/micropython/esp32/modifunneopixel.c#L100>`_
    - `Определение типа класса <https://github.com/vrubel/MicroPython_ESP32_psRAM_LoBo/blob/57227919ffa93f426fb21212a5d949ea92f2d9f5/MicroPython_BUILD/components/micropython/esp32/modifunneopixel.c#L111>`_
    - `Таблица - список состава модуля <https://github.com/vrubel/MicroPython_ESP32_psRAM_LoBo/blob/57227919ffa93f426fb21212a5d949ea92f2d9f5/MicroPython_BUILD/components/micropython/esp32/modifunneopixel.c#L123>`_
    - `Определение модуля <https://github.com/vrubel/MicroPython_ESP32_psRAM_LoBo/blob/57227919ffa93f426fb21212a5d949ea92f2d9f5/MicroPython_BUILD/components/micropython/esp32/modifunneopixel.c#L132>`_


Включение кода модуля в сборку прошивки
---------------------------------------
Как вы могли заметить, в состав комита входит четыре файла:

.. image:: _static/own_module_files.png

Один файл кодом модуля (modifunneopixel.c) и еще три файла в которые были внесены изменения:

    - `MicroPython_BUILD/components/micropython/Kconfig.projbuild <https://github.com/vrubel/MicroPython_ESP32_psRAM_LoBo/commit/57227919ffa93f426fb21212a5d949ea92f2d9f5#diff-795e1d5bf10f51ee9eddc41eb1c703ba>`_. Добавляет пункт с упоминанием о нашем модуле в визуальный конфигуратор прошивки, что позволит пометить его галочкой в процессе конфигурирования, и как следствие включить в сборку прошивки.
    - `MicroPython_BUILD/components/micropython/component.mk <https://github.com/vrubel/MicroPython_ESP32_psRAM_LoBo/commit/57227919ffa93f426fb21212a5d949ea92f2d9f5#diff-94e633fbde11d82c27199e805a67edcf>`_. Указывает, какие файлы с исходным кодом должны быть скомпилированны для сборки модуля
    - `MicroPython_BUILD/components/micropython/esp32/mpconfigport.h <https://github.com/vrubel/MicroPython_ESP32_psRAM_LoBo/commit/57227919ffa93f426fb21212a5d949ea92f2d9f5#diff-25fd3b13f027f30e548fea0e02b031e2>`_. Указывает, что модуль встроен в прошивку 


Проверка работы модуля
----------------------
После внесения всех вышеприведенных изменений, можно сконфигурировать прошивку, указав что необходимо включить наш модуль в сборку, собрать прошивку, прошить чип, и проверить работу модуля.

.. note::

    Команды конфигурирования, сборки и т.п. прошивки, выполняются из директории ``MicroPython_BUILD``


Конфигурирование прошивки
*************************
Выполняем команду ``./BUILD.sh menuconfig`` и в конфигураторе проходим по пути *MicroPython->Modules*, где помечаем наш модуль на включение в сборку.

.. image:: _static/firmware_conf.png


Сборка прошивки
***************
Выполняем последовательно две команды:

    - ``./BUILD.sh clean`` Очистит директорию результата сборки от результатов предыдущего процесса сборки.
    - ``./BUILD.sh -v`` Выполнит сборку прошивки


Прошивка чипа
*************
Просто выполните команду ``./BUILD.sh flash``


Проверка работы модуля
**********************
Получим доступ к консоли интерпретатора питона, работающего на девайсе, выполнив команду ``./BUILD.sh monitor`` (выйти - Ctrl+])

В консоли интерпретатора выполним команду ``help('modules')`` и убедимся, что в списке встроенных моудей присуствует наш модуль *indicator*

.. image:: _static/help_modules.png

Далее выполним:

- импорт модуля
- убедимся что модуль содержит класс *IFunNeopixel*
- создадим экземпляр класса
- убедимся что он содержит метод *add*
- вызовем метод объекта
- выведем объект на печать

.. image:: _static/test_my_mod.png

