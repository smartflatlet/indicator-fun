sphinx-autobuild . _build/html
make latexpdf

# Для апдейта перевода
sphinx-build -b gettext . _build/gettext
sphinx-intl update -p _build/gettext -l en
# Переводим и собираем
sphinx-build -b html -D language=en . _build/html/en


