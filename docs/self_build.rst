**********************
Самостоятельная сборка
**********************

В этом разделе приведена полезная информация для настоящих джедаев - тех кто решился самостоятельно спаять и прошить девайс, имея на руках голую плату и горстку деталей.  
Информации будет минимум, т.к. джедайство подразумевает наличие необходимого багажа знаний. Но если будут вопросы, инфу дополним.

Самостоятельная пайка
#####################
Для самостоятельной пайки джедаю достаточно принципиальной схемы, но иногда, чтобы определить как правильно установить чип (а некоторые из них очень мелкие, и маркировку на них наносили силы зла), требуется заглянуть в даташит.

Дабы сэкономить ваше время, привожу фотографии уже впаянных чипов.


.. figure:: _static/light_sensor.jpg

    **Датчик освещенности**

.. figure:: _static/microphone.jpg

    **Микрофон**

.. figure:: _static/regulator_1_8.jpg

    **Регулятор 1.8V**

.. figure:: _static/lm358.jpg

    **Операционный усилитель**

.. figure:: _static/diode.jpg

    **Диод**


Минимум напаянных деталей, необходимый для прошивки модуля
**********************************************************

.. figure:: _static/min_parts_front.jpg

    **Верхняя сторона платы**

.. figure:: _static/min_parts_back.jpg

    **Нижняя сторона платы**

В принципе сразу после того, как напаян необходимый минимум, можно прошить модуль, а остаток деталей допаять после.


Самостоятельная прошивка
########################
Существует несколько путей достижения цели (вдутая в модуль прошивка с поддержкой микропитона и питоновские скрипты размещенные во флеше модуля), отличающиеся уровнем джедайства и требуемым на преодоление пути временем. От самостоятельной сборки прошивки из исходников Бориса (ударение видимо на "О") Ловошевича, спасибо ему за `замечательный проект <https://github.com/loboris/MicroPython_ESP32_psRAM_LoBo>`_, до простого скачивания образов (из моей репы на гитхабе) и вдувания их в железку при помощи скрипта. 

Жалко мне ваше время други, потому заготовил для вас последний вариант, а время ваше отожрет подготовка. Подразумевая что делать вы это будете на винде, нижеприведенное специально для вас *(хотя имхо настоящие джедаи из тмукса не вылазят)*

1. Сразу установите себе на тачку драйвер для чипа конвертера USB-UART, используемого в железке (CH340G). Поиск в гугле дает множество вариантов, я `качнул отсюда <https://all-arduino.ru/drajver-ch340g-dlya-arduino/>`_.
2. Устанавливаете Python (3й)
3. Устанавливаете утилиту-прошивалку `esptool <https://github.com/espressif/esptool>`_
4. Качаете архив с образами `indicator_fun.zip <https://github.com/vrubel/MicroPython_ESP32_psRAM_LoBo/raw/indicator-fun/MicroPython_BUILD/firmware/indicator_fun.zip>`_ и вдувающий их скрипт `flash.sh <https://github.com/vrubel/MicroPython_ESP32_psRAM_LoBo/raw/indicator-fun/MicroPython_BUILD/firmware/flash.sh>`_

Распаковываете архив, запускаете "Git Bash" и перемещаетесь внутрь папки с образами.  

.. image:: _static/git_bash.jpg

Подключаете девайс, смотрите на какой компорт он сел и запускаете скрипт flash.sh с указанием порта.

::

    ../flash.sh -p com3

После прошивки передергиваете (питание), коннектитесь Putty-ком к ком-порту на скорости 115200 и видите примерно следующее

.. image:: _static/repl_start.jpg

То есть, вы в питонячей консоли. *На скриншоте, железка уже получила айпишник, т.к. уже подключилась к домашнему вайфаю. У вас по началу железка поднимет свою точку доступа, и напишет айпишник железки в сети.* Сразу будет поднят telnet сервер, так что можно заходить туда, вместо использования usb шнура (login: **micro**, pwd: **python**)

Можете сразу прописать свою домашнюю точку вайфай, и железка после перезагрузки будет коннектиться к ней. Для этого в питонячей консоли вводите (указывая естественно ваши ssid и ключ сети):

::

    wifi.setWiFi("ssid","key")
    reset()

Итак, что имеем? 

- Вдута правильная прошивка и образ файловой системы с уже размещенными в ней питоновскими скриптами (`из ветки мастер репы indicator.fun <https://bitbucket.org/smartflatlet/indicator-fun/src/master/app/>`_). 
- Теперь вы можете заливать скрипты либо через FTP, либо через USB шнурок при помощи утилиты `rshell <https://github.com/dhylands/rshell>`_ (подсмотреть как она вызывается можно в скрипте **indicator-fun/app/copy2flash.sh**. Этот скрипт забрасывает в железку файлы из папки *app*)

Все. Как увидите (напаяв светодиоды) сразу после старта, пока устанавливается вайфай соединение с точкой и поднимаются telnet, ftp и web сервера, железка радует эффектом "бегущий по кругу огонек". После старта серверов, кратковременно моргает всеми светодиодами (удобно визуально проверять, если где непропай.)

Также *(для удобства, в main.py временно создан объект шины i2c)* в питонячей консоли можно набрать:

::

    i2c.scan()

И получить список обнаруженных на шине датчиков (также удобно контролировать удачность пайки этой милюзги).  

    - 25 - аксель
    - 41 - датчик света (92 для 1750)
    - (не помню) - CO2 и температура\\влажность


