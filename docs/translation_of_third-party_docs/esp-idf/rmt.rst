Драйвер RMT (Remote Control)
****************************
`Оригинал статьи здесь <https://docs.espressif.com/projects/esp-idf/en/latest/api-reference/peripherals/rmt.html>`_

Драйвер RMT (Remote Control) предназначен для передачи и приема сигналов инфракрасных пультов управления, но благодаря гибкости реализации, может использоваться для генерации или приема других типов сигналов.

Сигнал, состоящий из серии импульсов, генерируется передатчиком RMT на основе списка значений. Значения определяют длительность импульса и его уровень (0 или 1). Передатчик также может генерировать несущую, которая модулируется генерируемым сигналом.

.. blockdiag::
    :scale: 100
    :caption: RMT Передатчик 
    :align: center

    blockdiag rmt_tx {

        node_width = 80;
        node_height = 60;
        default_group_color = lightgrey;

        a -> b -> c -> d;
        e -> f -> g -- h;
        d -> o [label=GPIO];
        h -> d [folded];

        a [style=none, width=100, label="{11,high,7,low},\n{5,high,5,low},\n..."]
        b [label="Генератор\nсигнала"]
        c [style=none, label="", background="_static/rmt-waveform.png"]
        d [shape=beginpoint, label="модуляция"]
        e [style=none, width=60, height=40, label="Разрешить\nнесущую"]
        f [label="Генератор\nнесущей"]
        g [style=none, label="", background="_static/rmt-carrier.png"]
        h [shape=none]
        o [style=none, label="", background="_static/rmt-waveform-modulated.png"]

        group {
            label = "На входе"
            a,e;
        }
        group {
            label = "RMT Передатчик"
            b,f,c,g,d,h;
        }
        group {
            label = "На выходе" 
            o;
        }
    }

Обратная операция выполняется приемником, где серия импульсов декодируется в список значений, содержащих длительность импульса и его уровень. Для удаления высокочастотного шума из входного сигнала может применяться фильтр.

.. blockdiag::
    :scale: 90
    :caption: RMT Приемник 
    :align: center

    blockdiag rmt_rx {

        node_width = 80;
        node_height = 60;
        default_group_color = lightgrey;

        a -> b [label=GPIO];
        b -> c -> d;
        e -- f;
        f -> b [folded];

        a [style=none, label="", background="_static/rmt-waveform.png"]
        b [label=Фильтр]
        c [label="Детектор\nфронтов"]
        d [style=none, width=100, label="{11,high,7,low},\n{5,high,5,low},\n..."]
        e [style=none, width=60, height=40, label="Разрешить\nфильтрацию"]
        f [shape=none, label=""]

        group {
            label = "На входе" 
            a,e;
        }
        group {
            label = "RMT Приемник"
            b,c;
        }
        group {
            label = "На выходе" 
            d;
        }
    }

Типичные операции по настройке и управлению драйвером RMT, описываются в следующих разделах:

1. `Конфигурирование драйвера`_
2. `Передача данных`_ или `Прием данных`_
3. `Изменение параметров функционирования`_
4. `Использование прерываний`_

RMT имеет восемь каналов, пронумерованных от нуля до семи и идентифицируемых константой из перечисления rmt_channel_t_. Каждый канал может быть использован для приема или передачи данных. 


Конфигурирование драйвера
-------------------------

Конфигурирование драйвера производится путем присваивания значений параметров, соответствующим элементам структуры rmt_config_t_. Некоторые параметры являются общими для режимов передачи и приема, а некоторые используются для конфигурирования только определенного режима.


Параметры, общие для обоих режимов работы (приема и передачи)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* **channel** устанавливает номер канала (присваиваемое значение - константа из перечисления rmt_channel_t_)
* **rmt_mode** определяет режим работы канала (присваиваемое значение - константа из перечисления rmt_mode_t_)
* **gpio_num** определяет номер вывода, используемого для приема или передачи
* **mem_block_num** определяет, сколько блоков памяти будет использоваться каналом
* **clk_div** Делитель тактовой частоты, определяющий длительность импульсов, генерируемых передатчиком или детектируемых приемником. Диапазон значений делителя [1..255]. По умолчанию, источником тактовых испульсов является APB CLK, с частотой 80Mhz. 


.. note::

    Длительность импульсов генерируемая передатчиком (или детектируемая приемником), кратна значению (длительности одного "тика"), полученному после деления тактовой частоты


Параметры режима передачи
^^^^^^^^^^^^^^^^^^^^^^^^^
При конфигурировании канала на передачу, следует установить элементы структуры rmt_tx_config_t_, представленной в общей структуре конфигурации элементом **tx_config**

* **loop_en** Разрешить циклическую передачу сконфигурированной последовательности импульсов
* **carrier_en** Разрешить генерацию несущей частоты
* **carrier_freq_hz** Частота несущей в герцах
* **carrier_duty_percent** Скважность импульсов несущей в процентах
* **carrier_level** Уровень сигнала заполняемый несущей (0 или 1)
* **idle_output_en** Разрешить подачу напряжения на выход, при простое передатчика RMT
* **idle_level** Уровень на выходе при простое передатчика (если разрешено)


Параметры режима приема
^^^^^^^^^^^^^^^^^^^^^^^

При конфигурировании канала на прием, следует установить элементы структуры rmt_rx_config_t_, представленной в общей структуре конфигурации элементом **rx_config**:

* **filter_en** Разрешить использование фильтра на входе RMT приемника
* **filter_ticks_thresh** Порог фильтрации, заданный в виде длительности тика. Импульсы имеющие длительность меньше, чем задано данным параметром, будут отфильтрованы. Значение задается в диапазоне [0..255].
* **idle_threshold** Длительность импульса, которая переводит приемник в режим ожидания (задается количеством тиков). Приемник будет игнорировать импулсы, длительность которых превышает значение установленное данным параметром.


Завершение конфигурирования
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Для применения настроек конфигурации необходимо вызвать функцию `rmt_config()`_, передав ей в качестве параметра заполненную структуру rmt_config_t_. 

Последний шаг конфигурации инициализация драйвера посредством вызова функции `rmt_driver_install()`_. Если параметр **rx_buf_size**, переданный этой функции, больше нуля, тогда будет выделено указанное кол-во памяти под кольцевой буфер. Будет установлен обработчик прерывания "по умолчанию" (см. примечание в разделе `Использование прерываний`_).


Передача данных
---------------

Прежде чем начать генерировать импульсы, необходимо определить шаблон по которому они будут генерироваться. Минимальный шаблон, распознаваемый RMT, называется элементом RMT и описывается структурой **rmt_item32_t**, см. определение структуры в файле `rmt_struct.h <https://github.com/espressif/esp-idf/blob/0d7f2d7/components/soc/esp32/include/soc/rmt_struct.h>`_.
Структура состоит из двух частей, каждая из которых содержит два значения. Первое значение в каждой из частей (представлено 15 битами) - длительность сигнала, а второе значение (1 бит) - уровень сигнала (низкий или высокий).

Ниже представлен блок из нескольких таких элементов и структура каждого элемента.

.. packetdiag::
    :caption: Структура элементов RMT (L - уровень сигнала)
    :align: center

    packetdiag rmt_items {
        colwidth = 32  
        node_width = 10
        node_height = 24
        default_fontsize = 12

        0-14: Period (15)
        15: L
        16-30: Period (15)
        31: L
        32-95: ... [colheight=2]
        96-110: Period (15)
        111: L
        112-126: Period (15)
        127: L
    }

В качестве демонстрации, как определить блок элементов, смотрите пример реализации: `peripherals/rmt_tx <https://github.com/espressif/esp-idf/blob/0d7f2d77c2eee51dce3fa7c8430a641c65cd9719/examples/peripherals/rmt_tx/main/rmt_tx_main.c>`_.

Элементы (в виде заполненных структур), передаются контроллеру RMT посредством функции **rmt_write_items()**. Данная функция также инициирует процесс генерации сигнала. В зависимости от параметров, она может либо ожидать окончания процесса генерации сигнала, либо стартовав его, вернуть управление. Во втором случае вы можете дождаться окончания процесса генерации, вызвав функцию **rmt_wait_tx_done()**. Использование данной функции не лимитирует кол-во обрабатываемых элементов, т.к. она использует прерывание для копирования данных частями, по мере того, как они обрабатываются контроллером RMT.

Другой способ предоставления данных контроллеру - вызов функции **rmt_fill_tx_items()**. В этом случае генерация сигнала не стартует автоматически. Для контроля данного процесса используются функции **rmt_tx_start()** и **rmt_tx_stop()**. Количество обрабатываемых элементов огранивается размером внутренней памяти контроллера RMT. см. описание функции **rmt_set_mem_block_num()**.


Прием данных
------------

Before starting the receiver we need some storage for incoming items. The RMT controller has 512 x 32-bits of internal RAM shared between all eight channels. In typical scenarios it is not enough as an ultimate storage for all incoming (and outgoing) items. Therefore this API supports retrieval of incoming items on the fly to save them in a ring buffer of a size defined by the user. The size is provided when calling :cpp:func:`rmt_driver_install` discussed above. To get a handle to this buffer call :cpp:func:`rmt_get_ringbuf_handle`.

With the above steps complete we can start the receiver by calling :cpp:func:`rmt_rx_start` and then move to checking what's inside the buffer. To do so, you can use common FreeRTOS functions that interact with the ring buffer. Please see an example how to do it in :example:`peripherals/rmt_nec_tx_rx`.

To stop the receiver, call :cpp:func:`rmt_rx_stop`.


Изменение параметров функционирования
-------------------------------------

Previously described function :cpp:func:`rmt_config` provides a convenient way to set several configuration parameters in one shot. This is usually done on application start. Then, when the application is running, the API provides an alternate way to update individual parameters by calling dedicated functions. Each function refers to the specific RMT channel provided as the first input parameter. Most of the functions have `_get_` counterpart to read back the currently configured value.


Parameters Common to Transmit and Receive Mode
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Selection of a GPIO pin number on the input or output of the RMT - :cpp:func:`rmt_set_pin`
* Number of memory blocks allocated for the incoming or outgoing data - :cpp:func:`rmt_set_mem_pd`
* Setting of the clock divider - :cpp:func:`rmt_set_clk_div`
* Selection of the clock source, note that currently one clock source is supported, the APB clock which is 80Mhz - :cpp:func:`rmt_set_source_clk`


Transmit Mode Parameters
^^^^^^^^^^^^^^^^^^^^^^^^

* Enable or disable the loop back mode for the transmitter - :cpp:func:`rmt_set_tx_loop_mode`
* Binary level on the output to apply the carrier - :cpp:func:`rmt_set_tx_carrier`, selected from :cpp:type:`rmt_carrier_level_t`
* Determines the binary level on the output when transmitter is idle - :cpp:func:`rmt_set_idle_level()`, selected from :cpp:type:`rmt_idle_level_t`


Receive Mode Parameters
^^^^^^^^^^^^^^^^^^^^^^^

* The filter setting - :cpp:func:`rmt_set_rx_filter`
* The receiver threshold setting - :cpp:func:`rmt_set_rx_idle_thresh`
* Whether the transmitter or receiver is entitled to access RMT's memory - :cpp:func:`rmt_set_memory_owner`, selection is from :cpp:type:`rmt_mem_owner_t`.


Использование прерываний
------------------------

Registering of an interrupt handler for the RMT controller is done be calling :cpp:func:`rmt_isr_register`. 

.. note::

    When calling :cpp:func:`rmt_driver_install` to use the system RMT driver, a default ISR is being installed. In such a case you cannot register a generic ISR handler with :cpp:func:`rmt_isr_register`. 

The RMT controller triggers interrupts on four specific events describes below. To enable interrupts on these events, the following functions are provided:

* The RMT receiver has finished receiving a signal - :cpp:func:`rmt_set_rx_intr_en`
* The RMT transmitter has finished transmitting the signal - :cpp:func:`rmt_set_tx_intr_en`
* The number of events the transmitter has sent matches a threshold value :cpp:func:`rmt_set_tx_thr_intr_en`
* Ownership to the RMT memory block has been violated - :cpp:func:`rmt_set_err_intr_en`

Setting or clearing an interrupt enable mask for specific channels and events may be also done by calling :cpp:func:`rmt_set_intr_enable_mask` or :cpp:func:`rmt_clr_intr_enable_mask`.

When servicing an interrupt within an ISR, the interrupt need to explicitly cleared. To do so, set specific bits described as ``RMT.int_clr.val.chN_event_name`` and defined as a ``volatile struct`` in :component_file:`soc/esp32/include/soc/rmt_struct.h`, where N is the RMT channel number [0, 7] and the ``event_name`` is one of four events described above.

If you do not need an ISR anymore, you can deregister it by calling a function :cpp:func:`rmt_isr_deregister`.


Деинсталляция драйвера
----------------------

Если драйвер больше не требуется, его можно удалить, чтобы освободить выделенные ресурсы, вызвав функцию `rmt_driver_uninstall()`_.


Примеры приложений
------------------

* Простой пример передачи с использованием RMT: `peripherals/rmt_tx <https://github.com/espressif/esp-idf/blob/0d7f2d77c2eee51dce3fa7c8430a641c65cd9719/examples/peripherals/rmt_tx/main/rmt_tx_main.c>`_.
* Пример генерации и детектирования сигналов, по протоколу используемому в инфракрасных пультах дистанционного управления NEC: `peripherals/rmt_nec_tx_rx <https://github.com/espressif/esp-idf/blob/0d7f2d77c2eee51dce3fa7c8430a641c65cd9719/examples/peripherals/rmt_nec_tx_rx/main/infrared_nec_main.c>`_.


.. _rmt_channel_t: https://docs.espressif.com/projects/esp-idf/en/latest/api-reference/peripherals/rmt.html#_CPPv213rmt_channel_t
.. _rmt_config_t: https://docs.espressif.com/projects/esp-idf/en/latest/api-reference/peripherals/rmt.html#_CPPv212rmt_config_t
.. _rmt_mode_t: https://docs.espressif.com/projects/esp-idf/en/latest/api-reference/peripherals/rmt.html#_CPPv210rmt_mode_t
.. _rmt_tx_config_t: https://docs.espressif.com/projects/esp-idf/en/latest/api-reference/peripherals/rmt.html#_CPPv215rmt_tx_config_t
.. _rmt_rx_config_t: https://docs.espressif.com/projects/esp-idf/en/latest/api-reference/peripherals/rmt.html#_CPPv215rmt_rx_config_t
.. _rmt_config(): https://docs.espressif.com/projects/esp-idf/en/latest/api-reference/peripherals/rmt.html#_CPPv210rmt_configPK12rmt_config_t
.. _rmt_driver_install(): https://docs.espressif.com/projects/esp-idf/en/latest/api-reference/peripherals/rmt.html#_CPPv218rmt_driver_install13rmt_channel_t6size_ti
.. _rmt_driver_uninstall(): https://docs.espressif.com/projects/esp-idf/en/latest/api-reference/peripherals/rmt.html#_CPPv220rmt_driver_uninstall13rmt_channel_t
