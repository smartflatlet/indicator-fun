Создание собственного Python модуля на Си
*****************************************

`Оригинал статьи здесь <https://micropython-dev-docs.readthedocs.io/en/latest/adding-module.html>`_

Когда вы используете MicroPython, шансов на то, что вам понадобится собственная библиотека  написанная на C, больше, чем при использовании  других реализаций Python. Например, вы можете захотеть использовать существующую сишную библиотеку, или получить доступ к периферийным устройствам, которые по умолчанию не доступны из MicroPython, или же сделать что-то свое, более быстрое и требующим для работы меньше памяти. Для этого вам необходимо добавить в прошивку свой собственный C код, и в этой статье мы покажем вам, как это сделать, используя порт ESP8266 в качестве примера.

Добавление вашего собственного Си файла
=======================================

Чтобы добавить свой собственный модуль MicroPython, написанный на C, вам нужно создать новый файл C и упомянуть его в нескольких других файлах, чтобы он попал в сборку.

Прежде всего, вам нужно добавить его имя в ``Makefile``, в список файлов исходных кодов, присваиваемый переменной ``SRC_C``. Порядок перечисления файлов в списке не имеет значения.

.. code-block:: make

    SRC_C = \
            main.c \
            system_stm32.c \
            stm32_it.c \
            ...
            mymodule.c
            ...

Второй файл, в который вам нужно добавить имя вашего файла, - ``esp8266.ld``, который является картой памяти, используемой компилятором. Вы должны добавить его в список в разделе ``.irom0.text``, чтобы ваш код попал в ROM. Если вы этого не сделаете, компилятор попытается поместить его в RAM, которая является очень скудным ресурсом и имеет привычку заканчиваться, если вы попытаетесь слишком много ее поиметь.

Теперь просто создайте пустой файл ``mymodule.c`` и запустите сборку, чтобы увидеть, что он используется при компиляции.


Создание Python модуля
======================

Теперь у вас есть файл, который на самом деле ни фига не делает. Он пустой, и естественно нет никакого нового модуля python, который вы могли бы импортировать в скриптах. Пора это изменить.

С точки зрения языка C, модули MicroPython - обыкновенные структуры. Поместите в файл ``mymodule.c`` следующий код:

.. code-block:: c

    #include "py/nlr.h"
    #include "py/obj.h"
    #include "py/runtime.h"
    #include "py/binary.h"
    #include "portmodules.h"

    STATIC const mp_map_elem_t mymodule_globals_table[] = {
        { MP_OBJ_NEW_QSTR(MP_QSTR___name__), MP_OBJ_NEW_QSTR(MP_QSTR_mymodule) },
    };

    STATIC MP_DEFINE_CONST_DICT ( 
        mp_module_mymodule_globals, 
        mymodule_globals_table
    );

    const mp_obj_module_t mp_module_mymodule = {
        .base = { &mp_type_module },
        .globals = (mp_obj_dict_t*)&mp_module_mymodule_globals,
    };

Что делает этот код? Он просто определяет python модуль, используя тип ``mp_obj_module_t``, и инициализирует некоторые из его полей, такие как базовый тип, имя, и словарь глобальных переменных модуля. В словаре определена одна переменная ``__name__``, содержащая имя самого модуля.

Для того чтобы модуль стал доступен для импорта, его стоит упомянуть в дефайне ``MICROPY_PORT_BUILTIN_MODULES``, в файле ``mpconfigport.h``:

.. code-block:: c

    extern const struct _mp_obj_module_t mp_module_mymodule;

    #define MICROPY_PORT_BUILTIN_MODULES \
        { MP_OBJ_NEW_QSTR(MP_QSTR_umachine), (mp_obj_t)&machine_module }, \
        ...
        { MP_OBJ_NEW_QSTR(MP_QSTR_mymodule), (mp_obj_t)&mp_module_mymodule }, \

Теперь, скомпилировав и прошив прошивку в модуль, вы можете выполнить ``import mymodule`` и увидеть, что модуль импортируется.


Добавление функции
==================

Тепрь добавим в модуль простую функцию. Добавьте в ``mymodule.c``, сразу после инклудов, следующее:

.. code-block:: c

    #include <stdio.h>

    STATIC mp_obj_t mymodule_hello(void) {
        printf("Hello world!\n");
        return mp_const_none;
    }
    STATIC MP_DEFINE_CONST_FUN_OBJ_0(mymodule_hello_obj, mymodule_hello);

Данным определением, из функции ``mymodule_hello`` языка C, создается объект функции языка python.
Обратите внимание, что и как каждая функция python, возвращающая результат в виде структуры ``mp_obj_t``, наша функция также возвращает значение  (None, ранее определенное как константа) в виде данной структуры.

Теперь добавляем объект функции в модуль:

.. code-block:: c

    STATIC const mp_map_elem_t mymodule_globals_table[] = {
        { MP_OBJ_NEW_QSTR(MP_QSTR___name__), MP_OBJ_NEW_QSTR(MP_QSTR_mymodule) },
        { MP_OBJ_NEW_QSTR(MP_QSTR_hello), (mp_obj_t)&mymodule_hello_obj },
    };

.. note::

    Реальное имя под которым объект будет виден в модуле, - это суффикс добавляемый к ``MP_QSTR_``

Теперь, скомпилировав и прошив прошивку в модуль, вы сможете импортировать модуль и вызвать его функцию ``hello()``

Аргументы функции
=================

Мы использовали макрос ``MP_DEFINE_CONST_FUN_OBJ_0`` для определения функции не принимающей аргументов. Для определения функции принимающей один аргумент, воспользуемся макросом ``MP_DEFINE_CONST_FUN_OBJ_1``.

.. code-block:: c

    STATIC mp_obj_t mymodule_hello(mp_obj_t what) {
        printf("Hello %s!\n", mp_obj_str_get_str(what));
        return mp_const_none;
    }
    STATIC MP_DEFINE_CONST_FUN_OBJ_1(mymodule_hello_obj, mymodule_hello);

.. note::

    Функции языка C, стоящие за объектами функций языка python, должны принимать в качестве параметров только переменные типа ``mp_obj_t``

Данная функция использует С функцию ``printf`` для вывода строки. Параметр ``what`` преобразуется в строку при помощи функции ``mp_obj_str_get_str``

Обратите внимание, что функция ``mp_obj_str_get_str`` выбросит исключение на стороне python, если переданный ей объект не будет являться строкой в понятиях типов python. Это очень удобно.

Также имеется возможность определять функции с переменным числом параметров или с именованными параметрами. Примеры вы можете легко найти в уже реализованных модулях, включенных в Micropython. Здесь мы не будем это описывать.


Классы
======

Определение питоновского класса - структура C с некоторыми обязательными полями:

.. code-block:: c

    // определение таблицы глобальных переменных класса
    STATIC const mp_rom_map_elem_t mymodule_hello_locals_dict_table[] = { };
    STATIC MP_DEFINE_CONST_DICT(mymodule_hello_locals_dict, 
                                mymodule_hello_locals_dict_table);

    // создание объекта класса
    const mp_obj_type_t mymodule_helloObj_type = {
        // наследование от типа "type"
        { &mp_type_type },
         // определение имени класса "helloObj"
        .name = MP_QSTR_helloObj,
         // присвоение функции выводящей объект на печать
        .print = mymodule_hello_print,
         // присвоение функции конструктора 
        .make_new = mymodule_hello_make_new,
         // присвоение словаря глобальных переменных класса 
        .locals_dict = (mp_obj_dict_t*)&mymodule_hello_locals_dict,
    };

Как видите, для определения класса нужны две функции - конструктор, для создания экземпляра класса и функция выводящая на печать экземпляр класса. Мы приведем их пример чуть ниже, а пока представим структуру, содержащую данные экземпляра класса:

.. code-block:: c
    
    // Эта структура, для размещения данных каждого нового объекта (экземпляр нашего класса)
    typedef struct _mymodule_hello_obj_t {
        // здесь содержится информация о типе объекта
        mp_obj_base_t base;
        // член экземпляра объекта, определяемый нами
        uint8_t hello_number;
    } mymodule_hello_obj_t;

Мы определили C структуру, которая содержит информацию о типе объекта и одно числовое поле ``hello_number``. 
Далее покажем как определяются конструктор и функция печати экземпляра класса:

.. code-block:: c

    mp_obj_t mymodule_hello_make_new( const mp_obj_type_t *type, 
                                      size_t n_args, 
                                      size_t n_kw, 
                                      const mp_obj_t *args ) {
        // этот вызов проверяет кол-во переданных аргументов (min 1, max 1);
        // при ошибке - выброс python исключения
        mp_arg_check_num(n_args, n_kw, 1, 1, true); 
        // Создание экземпляра объекта (из нашей C структуры)
        mymodule_hello_obj_t *self = m_new_obj(mymodule_hello_obj_t);
        // указываем тип объекта 
        self->base.type = &mymodule_hello_type;
        // присваиваем атрибуту объекта, значение первого аргумента
        self->hello_number = mp_obj_get_int(args[0])
        return MP_OBJ_FROM_PTR(self);
    }


    STATIC void mymodule_hello_print( const mp_print_t *print, 
                                      mp_obj_t self_in, 
                                      mp_print_kind_t kind ) {
        // Получим ссылку на C структуру представляющую объект
        mymodule_hello_obj_t *self = MP_OBJ_TO_PTR(self_in);
        // выведем на печать номер 
        printf ("Hello(%u)", self->hello_number);
    }

Теперь добавим наш объект класса в модуль, путем добавления последнего в таблицу глобальных членов модуля:

.. code-block:: c

    STATIC const mp_map_elem_t mymodule_globals_table[] = {
        { MP_OBJ_NEW_QSTR(MP_QSTR___name__), MP_OBJ_NEW_QSTR(MP_QSTR_mymodule) },
        { MP_OBJ_NEW_QSTR(MP_QSTR_hello), (mp_obj_t)&mymodule_hello_obj },
        { MP_OBJ_NEW_QSTR(MP_QSTR_helloObj), (mp_obj_t)&mymodule_helloObj_obj },
    };

Обратите внимание, что и функция ``mymodule_hello_obj``, добавленная в модуль ранее, и класс ``mymodule_helloObj_obj``, представлены в таблице глобальных членов модуля как значения типа ``mp_obj_t``.

Добавление методов
==================

Методы в микропитоне это просто объекты функции, добавленные в словарь глобальных переменных класса.
Они создаются так же, как функции для модуля. Просто помните, что первый аргумент функции, это указатель на структуру экземпляра класса:

.. code-block:: c

    STATIC mp_obj_t mymodule_hello_increment(mp_obj_t self_in) {
        mymodule_hello_obj_t *self = MP_OBJ_TO_PTR(self_in);
        self->hello_number += 1;
        return mp_const_none;
    }
    MP_DEFINE_CONST_FUN_OBJ_1(mymodule_hello_increment_obj, 
                              mymodule_hello_increment);


Также не забудьте добавить объект функцию в словарь членов класса:

.. code-block:: c

    STATIC const mp_rom_map_elem_t mymodule_hello_locals_dict_table[] = {
        { MP_ROM_QSTR(MP_QSTR_inc), MP_ROM_PTR(&mymodule_hello_increment_obj) },
    }


Использование вашего модуля в микропитоне
=========================================

Теперь вы можете использовать модуль в микропитоне, после ребилда прошивки. Например вы можете выполнить нечто подобное:

.. code-block:: python

    import mymodule;
  
    mymodule.hello();
    a = mymodule.hellObj(12);
    print(mymodule);
    mymodule.inc();
    print(mymodule);
    
