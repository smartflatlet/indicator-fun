*******************************
Переводы сторонней документации
*******************************

Здесь находятся переводы некоторых разделов документации проектов, на основе которых реализован данный проект.

.. note::

    В основе данного проекта лежит проект `MicroPython for ESP32 with psRAM support <https://github.com/loboris/MicroPython_ESP32_psRAM_LoBo>`_, который в свою очередь базируется на проектах `ESP-IDF <https://docs.espressif.com/projects/esp-idf/en/latest/index.html>`_ и `MicroPython <http://docs.micropython.org/en/latest/>`_

.. toctree::
   :maxdepth: 1
   :caption: Содержание: 

   esp-idf/rmt
   micropython-dev-docs/adding-module
