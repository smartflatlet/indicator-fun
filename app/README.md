# indicator-fun Python scripts

These are the Python script that run on the device itself.

## Development environment setup

The scripts require Python 3. Some systems, such as MacOS, have Python 2 installed by default. To get Python 3 install [direnv](https://github.com/direnv/direnv). Upon the first entry into `app` directory, a [virtual environment](https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/) with Python 3 will be installed under `.direnv` catalog and activated automatically.

### VS.Code

- Install [Micropython IDE](https://marketplace.visualstudio.com/items?itemName=dphans.micropython-ide-vscode) extension and follow the setup steps.

**Hints:**

You may need to hardcode the path to your local `ampy` and `rshell` into the created `.micropythonrc` file (as returned from `which ampy`, etc.

You may also want to create `.ampy` file in your working directory as outlined in the [ampy doc](https://github.com/pycampers/ampy)


