from microWebSrv import MicroWebSrv
from machine import reset, Timer
import wifi
import ujson
import leds
import re

# ----------------------------------------------------------------------------


def _httpHandlerSetLed(httpClient, httpResponse):
    """Set colors of all LEDs to the given RGB color.
       The color is passed in "allRgb" field of JSON POST payload.
    """
    print("In setled handler...")
    try:
        params = httpClient.ReadRequestContentAsJSON()
        leds.setAllRGB(params["allRgb"])
        httpResponse.WriteResponseOk()
    except Exception as e:
        message = str(e)
        print("Error: {}".format(message))
        httpResponse.WriteResponseJSONError(
            500, ujson.dumps({"error": message}))


def _httpHandlerSetNetwork(httpClient, httpResponse):
    formData = httpClient.ReadRequestPostedFormData()
    ssid = formData["ssid"]
    passphrase = formData["passphrase"]
    # TODO: Проверить длину и валидность имени и пароля в соответствии со стандартами
    wifi.setWiFi(ssid, passphrase)


def _httpHandlerReset(httpClient, httpResponse):

    # Get client request total path
    print("Get client request total path", httpClient.GetRequestTotalPath())

    # reset()


def web_callback(webSocket, data):
    webSocket.SendText(ujson.dumps(data))


def _acceptWebSocketCallback(webSocket, httpClient):
    print("WS ACCEPT")
    webSocket.RecvTextCallback = _recvTextCallback
    webSocket.RecvBinaryCallback = _recvBinaryCallback
    webSocket.ClosedCallback = _closedCallback
    # ctrl.RegWebCallback(web_callback, webSocket)


def _recvTextCallback(webSocket, msg):
    obj = ujson.loads(msg)
    cmd = obj.get("cmd")
    if cmd != None:
        if cmd == "setWiFi":
            wifi.setWiFi(obj.get("ssid"), obj.get("passphrase"))

        if cmd == "setColorHSB":
            leds.setAllHSB(obj.get("H"), obj.get("S"), obj.get("B"),)
            print(obj)


def _recvBinaryCallback(webSocket, data):
    print("WS RECV DATA : %s" % data)


def _closedCallback(webSocket):
    print("WS CLOSED")
    # ctrl.UnregWebCallback()


def setup():
    """Set up the web server"""
    routeHandlers = [
        ("/setled", "POST", _httpHandlerSetLed),
        ("/setnetwork", "POST",	_httpHandlerSetNetwork),
        ("/(.)*", "GET",	_httpHandlerReset)
    ]
    print("Starting web service...")
    srv = MicroWebSrv(routeHandlers=routeHandlers)
    srv.MaxWebSocketRecvLen = 256
    srv.WebSocketThreaded = True
    srv.AcceptWebSocketCallback = _acceptWebSocketCallback
    srv.Start(threaded=True)
